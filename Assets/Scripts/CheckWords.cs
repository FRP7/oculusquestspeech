﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CheckWords : MonoBehaviour
{
    [SerializeField] private AzureSpeech azurespeech;
    [SerializeField] private string[] keywords;

    private void Update() {
        VerifyWords();
    }

    private void VerifyWords() {
        /*if(azurespeech.message.Contains(keywords[0])) {
            Debug.Log("Peixe");
        }*/
        if (azurespeech.message.Contains(keywords[1])) {
            Debug.Log("Pára");
            azurespeech.Disable();
        }
    }
}
